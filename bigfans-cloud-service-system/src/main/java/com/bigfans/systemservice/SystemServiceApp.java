package com.bigfans.systemservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SystemServiceApp {

    public static void main(String[] args){
        SpringApplication.run(SystemServiceApp.class , args);
    }

}
