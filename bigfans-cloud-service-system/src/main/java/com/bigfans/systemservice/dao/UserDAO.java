package com.bigfans.systemservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.systemservice.model.User;

import java.math.BigDecimal;

public interface UserDAO extends BaseDAO<User> {
	
	int countByUsername(String account);
	
}