package com.bigfans.systemservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.systemservice.dao.UserDAO;
import com.bigfans.systemservice.model.User;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository(UserDAOImpl.BEAN_NAME)
public class UserDAOImpl extends MybatisDAOImpl<User> implements UserDAO {

	public static final String BEAN_NAME = "userDAO";
	
	public int countByUsername(String account){
		ParameterMap params = new ParameterMap();
		params.put("account", account);
		return getSqlSession().update(className + ".count", params);
	}

}