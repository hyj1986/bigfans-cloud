package com.bigfans.searchservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.searchservice.model.Brand;


public interface BrandDAO extends BaseDAO<Brand> {
	
}
