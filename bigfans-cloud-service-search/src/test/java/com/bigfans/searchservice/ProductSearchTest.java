package com.bigfans.searchservice;

import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.searchservice.model.Product;
import com.bigfans.searchservice.model.ProductSearchRequest;
import com.bigfans.searchservice.service.ProductSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-17 上午10:26
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchServiceApp.class)
public class ProductSearchTest {

    @Autowired
    private ProductSearchService productSearchService;

    @Test
    public void testSearch(){
        try {
            ProductSearchRequest searchRequest = new ProductSearchRequest();
            searchRequest.setKeyword("益达");
            FTSPageBean<Product> productPage = productSearchService.searchProduct( searchRequest, 0 , 10);
            List<Product> products = productPage.getData();
            System.out.println(products.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
