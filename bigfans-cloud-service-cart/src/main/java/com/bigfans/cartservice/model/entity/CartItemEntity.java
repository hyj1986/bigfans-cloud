package com.bigfans.cartservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 购物项
 * 
 * @author lichong
 *
 */
@Data
@Table(name="CartItem")
public class CartItemEntity extends AbstractModel {

	private static final long serialVersionUID = 1077289062261500652L;
	@Column(name="prod_id")
	protected String prodId;
	@Column(name="quantity")
	protected Integer quantity;
	@Column(name="user_id")
	protected String userId;
	@Column(name="is_selected")
	protected Boolean isSelected;
	
	public String getModule() {
		return "CartItem";
	}

}
