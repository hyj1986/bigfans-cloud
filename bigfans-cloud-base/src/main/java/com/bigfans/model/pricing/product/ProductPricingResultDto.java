package com.bigfans.model.pricing.product;

import java.util.HashMap;
import java.util.Map;

public class ProductPricingResultDto {

    private Map<String , ProductPricingItemDto> priceMap = new HashMap<>();

    public void setPrice(String prodId , ProductPricingItemDto productPricingItemDto){
        this.priceMap.put(prodId , productPricingItemDto);
    }

    public ProductPricingItemDto getPrice(String prodId){
        return this.priceMap.get(prodId);
    }

}
