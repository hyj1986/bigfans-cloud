package com.bigfans.notificationservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.order.OrderCreatedEvent;
import com.bigfans.model.event.payment.OrderPaidEvent;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@KafkaConsumerBean
public class OrderListener {

    private ScheduledExecutorService scheduledExecutorService;

    public OrderListener() {
        scheduledExecutorService = Executors.newScheduledThreadPool(10);
    }

    @KafkaListener
    public void on(OrderCreatedEvent event){
        scheduledExecutorService.schedule(new Runnable() {
            @Override
            public void run() {

            }
        } , 20, TimeUnit.MINUTES);

    }

    @KafkaListener
    public void on(OrderPaidEvent event){
        String orderId = event.getOrderId();
    }

}
