package com.bigfans.framework.es;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年2月12日上午9:18:34
 *
 */
public class ElasticSearchException extends RuntimeException {

	private static final long serialVersionUID = 6787103056884529031L;

	public ElasticSearchException(Throwable th) {
		super(th);
	}
	
	public ElasticSearchException(String message) {
		super(message);
	}
}
