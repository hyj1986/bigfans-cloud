package com.bigfans.framework.dao;

import com.bigfans.framework.model.AbstractModel;

import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 模型装饰器,扩展单表crud功能
 * @author lichong 
 * @date 2016年1月13日 上午10:19:09 
 * @version V1.0
 */
public class BeanDecorator {
	
	private WrappedJdbcDAO dao;
	private AbstractModel model;
	private List<? extends AbstractModel> modelList;
	private Long start;
	private Long pagesize;
	
	public BeanDecorator() {
		dao = WrappedJdbcDAO.getInstance();
	}
	
	public <T extends AbstractModel> BeanDecorator(T model) {
		this();
		this.model = model;
	}
	
	public <T extends AbstractModel> BeanDecorator(List<T> modelList) {
		this();
		this.modelList = modelList;
	}
	
	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getPagesize() {
		return pagesize;
	}

	public void setPagesize(Long pagesize) {
		this.pagesize = pagesize;
	}

	public <T extends AbstractModel> int insert(){
		int count = dao.insert(model).intValue();
		return count;
	}
	
	public <T extends AbstractModel> Long count(){
		return dao.count(model).longValue();
	}
	
	public <T extends AbstractModel> int batchInsert(){
		int count = dao.batchInsert(modelList).intValue();
		return count;
	}
	
	public <T extends AbstractModel> int delete(){
		int count = dao.delete(model).intValue();
		return count;
	}
	
	public <T extends AbstractModel> int update(){
		int count = dao.update(model).intValue();
		return count;
	}
	
	public <T extends AbstractModel> T load(String id , Class<T> modelType){
		T model = null;
		try {
			model = modelType.newInstance();
			model.setId(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dao.load(model);
	}
	
	public <T extends AbstractModel> T load(){
		return dao.load(model);
	}
	
	public <T extends AbstractModel> List<T> list(){
		return (List<T>) dao.list(model, start , pagesize);
	}

//	public int saveFtsCandidate(){
//		boolean annotationPresent = model.getClass().isAnnotationPresent(FtsEnable.class);
//		if(!annotationPresent){
//			return 0;
//		}
//		FtsCandidate candidate = new FtsCandidate();
//		candidate.setBeanId(model.getId());
//		candidate.setBeanModule(model.getModule());
//		candidate.setProcessed(false);
//		return new BeanDecorator(candidate).insert();
//	}
//
//	public int saveFtsCandidates(){
//		List<FtsCandidate> candidates = new ArrayList<FtsCandidate>();
//		for(AbstractModel m : this.modelList){
//			boolean annotationPresent = m.getClass().isAnnotationPresent(FtsEnable.class);
//			if(!annotationPresent){
//				continue;
//			}
//			FtsCandidate candidate = new FtsCandidate();
//			candidate.setBeanId(m.getId());
//			candidate.setBeanModule(m.getModule());
//			candidate.setProcessed(false);
//			candidates.add(candidate);
//		}
//		if(CollectionUtils.isEmpty(candidates)){
//			return 0;
//		}
//		return new BeanDecorator(candidates).batchInsert();
//	}

}
