package com.bigfans.catalogservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;
import lombok.Data;

@Data
public class OutOfStockException extends ServiceRuntimeException {

    private String prodId;

    public OutOfStockException(String prodId) {
        this.prodId = prodId;
    }
}
