package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.TagDAO;
import com.bigfans.catalogservice.model.Tag;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年3月1日 上午8:05:32 
 * @version V1.0
 */
@Repository(TagDAOImpl.BEAN_NAME)
public class TagDAOImpl extends MybatisDAOImpl<Tag> implements TagDAO {

	public static final String BEAN_NAME = "tagDAO";

	@Override
	public List<Tag> listByProdId(String prodId) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public List<Tag> listByContent(String name) {
		ParameterMap params = new ParameterMap();
		params.put("name", name);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public int updatePgRelatedDuplicates(List<String> oldTagIdList, String newId) {
		ParameterMap params = new ParameterMap();
		params.put("oldTagIdList", oldTagIdList);
		params.put("newId", newId);
		return getSqlSession().selectOne(className + ".mergeDuplicates", params);
	}
	
	@Override
	public Integer increaseRelatedCount(String tagId, Integer increase) {
		return null;
	}
}
