package com.bigfans.orderservice.api.clients;

import com.bigfans.framework.utils.ArithUtils;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.model.dto.cart.CartItemCouponDto;
import com.bigfans.model.dto.cart.CartItemPricingResultDto;
import com.bigfans.model.dto.cart.CartItemPromotionDto;
import com.bigfans.model.dto.cart.CartPricingResultDto;
import com.bigfans.model.dto.order.*;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-15 下午6:54
 **/
@Component
public class PricingServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "calculateOrderFallback")
    public CompletableFuture<OrderPricingResultDto> calculateOrder(OrderPricingDto orderPricingDto){
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://pricing-service/calculateOrder").build().encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.postForEntity(builder.toUri(), orderPricingDto, RestResponse.class);
            RestResponse restResponse = responseEntity.getBody();
            Map data = (Map)restResponse.getData();
            OrderPricingResultDto calculateResultDto = new OrderPricingResultDto();
            calculateResultDto.setProdTotalQuantity((Integer)data.get("prodTotalQuantity"));
            calculateResultDto.setTotalPrice(ArithUtils.toBigDecimal(data.get("totalPrice")));
            calculateResultDto.setOriginalTotalPrice(ArithUtils.toBigDecimal(data.get("originalTotalPrice")));
            calculateResultDto.setFreight(ArithUtils.toBigDecimal(data.get("freight")));
            calculateResultDto.setCouponDeductionTotal(ArithUtils.toBigDecimal(data.get("couponDeductionTotal")));
            calculateResultDto.setPointDeductionTotal(ArithUtils.toBigDecimal(data.get("pointDeductionTotal")));
            Map priceMap = (Map)data.get("priceMap");
            for(Object key : priceMap.keySet()){
                Map priceItem = (Map)priceMap.get(key);
                OrderItemPricingResultDto itemPricingResultDto = BeanUtils.mapToModel(priceItem, OrderItemPricingResultDto.class);
                calculateResultDto.addItemResult(itemPricingResultDto);
            }

            Map promotionMap = (Map)data.get("promotionMap");
            for(Object key : promotionMap.keySet()){
                List promotionItems = (List)promotionMap.get(key);
                for(Object pmtItem : promotionItems){
                    OrderItemPromotionDto cartItemPromotionDto = BeanUtils.mapToModel((Map) pmtItem, OrderItemPromotionDto.class);
                    calculateResultDto.addPromotion((String) key , cartItemPromotionDto);
                }
            }

            return calculateResultDto;
        });
    }

    public OrderPricingResultDto calculateOrderFallback(OrderPricingDto calculateOrderDto){
        OrderPricingResultDto dto = new OrderPricingResultDto();
        return dto;
    }
}
