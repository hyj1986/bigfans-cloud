package com.bigfans.orderservice.model;

import lombok.Data;

@Data
public class ProductSpec {

	private static final long serialVersionUID = -6811834593128832769L;

	protected String option;
	protected String value;
}
