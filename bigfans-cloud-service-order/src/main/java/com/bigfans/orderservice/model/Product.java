package com.bigfans.orderservice.model;

import com.bigfans.orderservice.model.entity.ProductEntity;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-16 下午7:54
 **/
@Data
public class Product extends ProductEntity {
}
